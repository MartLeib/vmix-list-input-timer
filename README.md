# vMix List input timer

Creates a web server that outputs JSON string containing time left for Video/List input which is in the program window. This JSON can be used in vMix using Data Source and then linked to title for example.